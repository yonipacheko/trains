class CreateAllData < ActiveRecord::Migration
  def change
    create_table :all_data do |t|

      t.integer :train_number
      t.string  :train_location
      t.datetime  :departure_time
      t.datetime  :arrival_time


    end
  end
end
