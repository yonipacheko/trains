class CreateTimeTables < ActiveRecord::Migration
  def change
    create_table :time_tables do |t|

      t.integer :train_number
      t.string :from_location
      t.string :to_location
      t.datetime :departure_time
      t.datetime :arrival_time

      t.timestamps
    end
  end
end
